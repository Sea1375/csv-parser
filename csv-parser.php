<?php

$delimiter = getDelimiter($argv[1]);
$list =(object) array();

$head = array("make", "model", "condition", "grade", "capacity", "colour", "network", "count");
$fr = fopen($argv[2], "r");

if ($fr !== FALSE) {

  $isFirstRow = true;
  while (!feof($fr)) {
    $data = fgets($fr, 2048);
    $line = str_getcsv($data, $delimiter);

    if ($isFirstRow) {
      $isFirstRow = false;
      continue;
    }

    $content = implode($line);
    $content = urlencode($content);

    array_push($line, 1);

    if (property_exists($list, $content)) {
      $len = count($list->$content);
      $list->$content[$len - 1]++;
    } else {
      $list = (object) array_merge((array)$list, array($content => $line));
    }
  }
  fclose($fr);

  foreach($list as $key => $value) {
    $filename = createFileName();
    $fp = fopen("output/".$filename.".csv", "w");
    fputcsv($fp, $head);
    fputcsv($fp, $value);
    fclose($fp);
  }
}

function createFileName() {
 return time()."-".mt_rand();
}

function getDelimiter($fileType) {
  if ($fileType === "csv") {
    return ",";
  } else if($fileType === "tsv") {
    return "\t";
  }
}
?>
